# Alexa Robot Control Skill - Server

## About this Project

This was a semester Project for the course "Entwicklung digitaler Sprachassistenten" (development of digital speech assistants) at the University of Applied Science Worms.

### Setup

Clone the repository:
```sh
git clone git@gitlab.com:98knobi/alexa-bot-server.git
```

Install all packages:
```sh
npm install
```

The Project is written in Typescript therefor it needs to be build.

The Post install script should build the Project automatically, after all packages are installed.

For manually building just run:
```sh
npm run build
```

### Startup

To start the Project simply execute:
```sh
npm start
```

### Generate the Code-Documentation

This Project uses Typedoc for generating a Code-Documentation.

To generate the Documentation simply execute:
```sh
npm run gendocs
```

After a short moment the Documentation should be generated in the `docs` folder.

To view the Docs simply open the `index.html` file in a Webbrowser.

## About the Robot

There is an separate project for the code of the Robot [here](https://gitlab.com/98knobi/alexa-bot).

The Robot basically consists of an ESP8266 based micocontroller development board, two steppers and a 3D printed housing.

## Further Documentation

I have created a German wiki Page with a Documentation about the Project creation and my Experience with it.

You can find the wiki page [here](https://gitlab.com/98knobi/alexa-bot-server/-/wikis/doku)