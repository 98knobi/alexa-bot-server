import { Server } from 'ws';
import { Service } from 'typedi';
import * as marked from 'marked';
import * as TerminalRenderer from 'marked-terminal';

marked.setOptions({
    renderer: new TerminalRenderer()
});

/**
 * This Conntroller is for sending the Commands to the Robot via a Websocket Connection
 *
 * It is defined as an Service and should be used via Dependency Injection
 *
 * That way there is only one instance needed for the whole app
 */
@Service()
export default class WebsocketController {
    private websocketServer = new Server({
        port: 1337
    });

    private _connectedClients: WebSocket[] = new Array();

    constructor() {
        this.websocketServer.on('connection', this.onConnectionHandler.bind(this));
        console.log(marked(`Websocket Server started and listening on \`http://localhost:${this.websocketServer.options.port}\``));
    }

    // adding all connected clients to one list
    private onConnectionHandler(socket: WebSocket, req) {
        this._connectedClients.push(socket);

        socket.onclose = this.onConnectionCloseHandler.bind(this);
    }

    private onConnectionCloseHandler(e) {
        const targetIdx = this.connectedClients.findIndex(client => client === e.target);
        this._connectedClients.splice(targetIdx, 1);
    }

    /**
     * Sends the given Command to all connected Websocket clients
     *
     * In theory multiple connected Robots would receive the same Commands
     * @param command
     */
    sendCommand(command: {
        command: string,
        steps: number
    }) {
        this._connectedClients.forEach((client) => {
            client.send(JSON.stringify(command));
        });
    }

    get connectedClients(): WebSocket[] {
        return this._connectedClients;
    }
}