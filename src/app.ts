/**
 * Alexa Robot Controll Server
 * Author: Simon Knobloch
 */

import 'colors';
import 'reflect-metadata';
import * as morgan from 'morgan';
import * as marked from 'marked';
import * as express from 'express';
import { Container } from 'typedi';
import * as packagedata from 'pjson';
import { SkillBuilders } from 'ask-sdk';
import * as TerminalRenderer from 'marked-terminal';
import { ExpressAdapter } from 'ask-sdk-express-adapter';

import LaunchRequestHandler from './RequestHandlers/LaunchRequestHandler';
import DriveForwardIntentHandler from './RequestHandlers/IntentHandlers/DriveForwardIntentHandler';
import DriveBackwardsIntentHandler from './RequestHandlers/IntentHandlers/DriveBackwardsIntentHandler';
import RotateClockwiseIntentHandler from './RequestHandlers/IntentHandlers/RotateClockwiseIntentHandler';
import RotateCounterClockwiseIntentHandler from './RequestHandlers/IntentHandlers/RotateCounterClockwiseIntentHandler';

marked.setOptions({
    renderer: new TerminalRenderer()
});

console.log(`Alexa Robot Controll Skill - Server v${packagedata.version}`.yellow.underline);
console.log('\nBy Simon Knobloch - 2020'.yellow.dim);
console.log(marked('___'));

const app = express();

app.use(morgan('combined'));

// setting up the Alexa app
const skillBuilder = SkillBuilders.custom();

skillBuilder.withSkillId('amzn1.ask.skill.dd988e82-dd0c-483c-a984-4a10902994e7');

// loading the Handlers via dependency Injection and adding them to the Skill
skillBuilder.addRequestHandlers(
    Container.get(LaunchRequestHandler),
    Container.get(DriveForwardIntentHandler),
    Container.get(DriveBackwardsIntentHandler),
    Container.get(RotateClockwiseIntentHandler),
    Container.get(RotateCounterClockwiseIntentHandler)
);

const skill = skillBuilder.create();
const adapter = new ExpressAdapter(skill, true, true);

app.post('/', adapter.getRequestHandlers()); // binding the Alexa Middleware to the root Route

if (!process.env.PORT)
    process.env.PORT = '8080';

app.listen(process.env.PORT, () => {
    console.log(marked(`Webserver started and listening on \`http://localhost:${process.env.PORT}\``));
});