const factor = 3;

export default function degreeToStep(degree: number): number {
    return degree * factor;
}