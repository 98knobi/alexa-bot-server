const factor = 12;

export default function cmToStep(length: number): number {
    return length * factor;
}